let http = require("http");

//Mock Data
let products = [

	{
		name: "Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name: "Horizon Forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 4000
	},
	{
		name: "Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}

];

http.createServer(function(req, res){

	console.log(req.url);
	console.log(req.method);

	if(req.url === "/products" && req.method === "GET"){
		res.writeHead(200, {"Content-type":"application/json"});
		res.end(JSON.stringify(products));
	}
	else if(req.url === "/products" && req.method === "POST"){
		
		let reqProducts = "";

		req.on(`data`, function(data){
			reqProducts += data;
		})

		req.on(`end`, function(){
			console.log(products);

			reqProducts = JSON.parse(reqProducts);

			let newProducts = {
				name: reqProducts.name,
				description: reqProducts.description,
				price: reqProducts.price
			};

			console.log(newProducts);
			products.push(newProducts);
			console.log(products);

			res.writeHead(201, {"Content-type":"application/json"});
			res.end(JSON.stringify(products));
		})

	}

}).listen(8000);

console.log(`Server running in 8000`);
